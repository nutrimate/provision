#!/usr/bin/env bash

set -e

PARENT_DIR="$(dirname "$0")/../.."

REPOSITORIES=(
  # Front-ends
  "landing"
  "dietitian-web-app"
  # Microservices
  "microservice-kit"
  "cookbook"
  "dietitian"
  # Functions
  "meal-planner"
)

for REPOSITORY in "${REPOSITORIES[@]}"
do
    if [ -d "${PARENT_DIR}/${REPOSITORY}" ]; then
        echo "🐨 Skipping $REPOSITORY"
    else
        echo "🐨 Pulling $REPOSITORY...";
        git clone "git@gitlab.com:nutrimate/${REPOSITORY}.git" "${PARENT_DIR}/${REPOSITORY}"
    fi
done

echo "Done 🎉";
