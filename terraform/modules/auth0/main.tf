terraform {
  required_providers {
    auth0 = {
      source  = "alexkappa/auth0"
      version = "0.15.2"
    }
  }
}

resource "auth0_tenant" "tenant" {
  friendly_name = "Nutrimate (stage)"
  support_email = "auth0@nutrimate.app"

  enabled_locales = ["pl"]
}
