resource "auth0_role" "dietitian" {
  name        = "dietitian"
  description = "Role for dietitians that allows to read and write diets"

  permissions {
    resource_server_identifier = auth0_resource_server.api.identifier
    name                       = local.permissions.cookbook.read
  }

  permissions {
    resource_server_identifier = auth0_resource_server.api.identifier
    name                       = local.permissions.cookbook.write
  }

  permissions {
    resource_server_identifier = auth0_resource_server.api.identifier
    name                       = local.permissions.dietitian.readDiets
  }

  permissions {
    resource_server_identifier = auth0_resource_server.api.identifier
    name                       = local.permissions.dietitian.writeDiets
  }
}
