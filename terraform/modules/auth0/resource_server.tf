resource "auth0_resource_server" "api" {
  name                                            = "api"
  identifier                                      = var.api_url
  enforce_policies                                = true
  token_dialect                                   = "access_token_authz"
  skip_consent_for_verifiable_first_party_clients = true

  scopes {
    description = "Grants read access to all cookbook resources."
    value       = local.permissions.cookbook.read
  }

  scopes {
    description = "Grants write access to all cookbook resources."
    value       = local.permissions.cookbook.write
  }

  scopes {
    description = "Grants read access to diets."
    value       = local.permissions.dietitian.readDiets
  }

  scopes {
    description = "Grants write access to diets."
    value       = local.permissions.dietitian.writeDiets
  }
}
