resource "auth0_client" "dietitian_web_app" {
  name = "Dietitian Web App"

  app_type = "spa"

  grant_types = [
    "authorization_code",
    "refresh_token",
  ]

  web_origins         = var.dietitian_web_app.web_origins
  callbacks           = var.dietitian_web_app.callback_urls
  allowed_logout_urls = var.dietitian_web_app.logout_urls

  oidc_conformant = true
  is_first_party  = true
}
