resource "auth0_connection" "default_username_password" {
  name     = "Username-Password-Authentication"
  strategy = "auth0"

  options {
    disable_signup = true
  }

  enabled_clients = [
    auth0_client.dietitian_web_app.id
  ]
}
