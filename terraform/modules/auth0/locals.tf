locals {
  permissions = {
    cookbook = {
      read  = "read:cookbook"
      write = "write:cookbook"
    }

    dietitian = {
      readDiets  = "read:diets"
      writeDiets = "write:diets"
    }
  }
}
