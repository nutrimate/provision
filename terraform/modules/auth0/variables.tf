variable "dietitian_web_app" {
  type = object({
    web_origins   = list(string)
    callback_urls = list(string)
    logout_urls   = list(string)
  })
  description = "dietitian-web-app valid redirects"
}

variable "api_url" {
  type = string
}

variable "auth_domain" {
  type = string
}
