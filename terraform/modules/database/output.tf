output "endpoint" {
  value = module.shared_db.this_db_instance_endpoint
}

output "port" {
  value = module.shared_db.this_db_instance_port
}

output "master_username" {
  value = module.shared_db.this_db_instance_username
}

output "master_password" {
  value = module.shared_db.this_db_instance_password

  sensitive = true
}

output "database_name" {
  value = module.shared_db.this_db_instance_name
}

output "security_group_id" {
  value = module.shared_db_security_group.this_security_group_id
}
