locals {
  instance_name = "shared-db"

  tags = merge(var.base_tags, {
    component : "shared-db"
  })
}
