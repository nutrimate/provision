resource "random_password" "shared_db_password" {
  length           = 16
  special          = true
  override_special = "!#$%^&*()-_+={}[]:;',.<>?"
}

module "shared_db" {
  source  = "terraform-aws-modules/rds/aws"
  version = ">= 2.20.0, < 3.0.0"

  identifier = local.instance_name

  engine               = "postgres"
  engine_version       = "12"
  family               = "postgres12"
  major_engine_version = "12"
  instance_class       = "db.t2.micro"
  allocated_storage    = 10

  name     = "shared"
  username = "nutrimate_admin"
  password = random_password.shared_db_password.result
  port     = "5432"

  subnet_ids = var.subnet_ids
  vpc_security_group_ids = [
    module.shared_db_security_group.this_security_group_id
  ]

  maintenance_window = "Sun:00:00-Sun:03:00"
  backup_window      = "03:00-06:00"

  backup_retention_period         = 3
  enabled_cloudwatch_logs_exports = ["postgresql", "upgrade"]

  final_snapshot_identifier = "${local.instance_name}-final-snapshot"

  deletion_protection = true

  tags = local.tags
}
