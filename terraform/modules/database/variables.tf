variable "vpc_id" {
  type = string
}

variable "subnet_ids" {
  type = list(string)
}

variable "allowed_ingress_security_groups" {
  type = list(string)
}

variable "base_tags" {
  type = map(string)
}
