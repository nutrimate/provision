module "shared_db_security_group" {
  source  = "terraform-aws-modules/security-group/aws//modules/postgresql"
  version = ">= 3.17.0, < 4.0.0"

  vpc_id = var.vpc_id
  name   = "${local.instance_name}-sg"

  auto_ingress_rules = []
  auto_egress_rules  = []

  ingress_with_source_security_group_id = [
    for security_group in var.allowed_ingress_security_groups :
    {
      rule                     = "postgresql-tcp"
      source_security_group_id = security_group
    }
  ]

  tags = local.tags
}
