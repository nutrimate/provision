locals {
  k3s_db_user             = "k3s_server"
  k3s_server_ssh_key_path = "${path.root}/k3s_server_ssh.pem"

  tags = merge(var.tags, { component = "k3s" })
}
