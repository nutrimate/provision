module "k3s_ingress_load_balancer_security_group" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "k3s-ingress-lb"
  description = "Security group for k3s ingress load balancer"
  vpc_id      = var.vpc_id

  ingress_cidr_blocks      = ["0.0.0.0/0"]
  ingress_ipv6_cidr_blocks = ["::/0"]
  ingress_rules            = ["http-80-tcp", "https-443-tcp"]

  egress_cidr_blocks      = var.k3s_server_cidr_blocks
  egress_ipv6_cidr_blocks = []
  egress_rules            = ["http-80-tcp", "https-443-tcp"]

  tags = local.tags
}

module "k3s_ingress_load_balancer" {
  source  = "terraform-aws-modules/elb/aws"
  version = "~> 2.4.0"

  name = "k3s-lb"

  subnets = var.lb_subnets
  security_groups = [
    module.k3s_ingress_load_balancer_security_group.this_security_group_id
  ]

  listener = [
    {
      instance_port     = "80"
      instance_protocol = "tcp"
      lb_port           = "80"
      lb_protocol       = "tcp"
    },
    {
      instance_port     = "443"
      instance_protocol = "tcp"
      lb_port           = "443"
      lb_protocol       = "tcp"
    }
  ]

  health_check = {
    target              = "HTTP:80/healthz"
    interval            = 30
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 5
  }

  tags = local.tags
}
