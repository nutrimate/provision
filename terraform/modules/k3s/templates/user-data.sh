#!/usr/bin/env bash

# Update the system

yum -y update

# Enable swap

dd if=/dev/zero of=/swapfile bs=128M count=64
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
swapon -s
echo "/swapfile swap swap defaults 0 0" >> /etc/fstab

# Enable auto-updates

crontab<<EOF
$(crontab -l)
0 2 * * *    yum -y update
EOF

# Install K3s

PUBLIC_IP=$(curl -s http://169.254.169.254/latest/meta-data/public-ipv4)

curl -sLS https://get.k3sup.dev | sh

k3sup install \
  --k3s-extra-args "--disable traefik --tls-san $${PUBLIC_IP}" \
  --local \
  --local-path /root/kubeconfig \
  --datastore="${DATASTORE}"

mkdir -p /home/ec2-user
cp /root/kubeconfig /home/ec2-user/kubeconfig
sed -i -e "s/127.0.0.1/$${PUBLIC_IP}/g" /home/ec2-user/kubeconfig
chown ec2-user /home/ec2-user/kubeconfig
