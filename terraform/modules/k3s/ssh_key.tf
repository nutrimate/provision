resource "tls_private_key" "k3s_server_ssh" {
  algorithm = "RSA"

  rsa_bits = 4096
}

resource "aws_key_pair" "k3s_server_ssh" {
  key_name   = "k3s-server-key"
  public_key = tls_private_key.k3s_server_ssh.public_key_openssh

  tags = local.tags
}

resource "local_file" "k3_server_private_key" {
  filename        = local.k3s_server_ssh_key_path
  file_permission = "0600"

  content = tls_private_key.k3s_server_ssh.private_key_pem
}
