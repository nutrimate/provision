module "k3s_database" {
  source = "./database"

  db_port = var.db_port

  db_security_group  = var.k3s_db_security_group
  k3s_security_group = module.k3s_server_security_group.this_security_group_id
}

data "template_file" "launch_k3s_server" {
  template = file("${path.module}/templates/user-data.sh")

  vars = {
    DATASTORE = "postgres://${module.k3s_database.username}:${urlencode(module.k3s_database.password)}@${var.db_endpoint}/${module.k3s_database.db_name}"
  }
}

module "k3s_servers" {
  source  = "terraform-aws-modules/autoscaling/aws"
  version = "~> 3.0"

  name    = "k3s-server"
  lc_name = "launch-k3s-server"

  image_id      = var.k3s_server_ami_id
  instance_type = var.k3s_server_instance_type
  root_block_device = [
    {
      volume_size = "30"
      volume_type = "gp2"
    }
  ]

  key_name = aws_key_pair.k3s_server_ssh.key_name

  vpc_zone_identifier = var.k3s_server_subnets
  security_groups = [
    module.k3s_server_security_group.this_security_group_id
  ]

  user_data = data.template_file.launch_k3s_server.rendered

  asg_name          = "k3s-server-asg"
  health_check_type = "EC2"
  min_size          = 1
  max_size          = 2
  desired_capacity  = 1

  load_balancers = [
    module.k3s_ingress_load_balancer.this_elb_id
  ]

  wait_for_capacity_timeout = "15m"

  tags_as_map = local.tags

  depends_on = [
    module.k3s_database
  ]
}
