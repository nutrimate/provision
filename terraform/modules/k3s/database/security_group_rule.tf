resource "aws_security_group_rule" "allow_ingress" {
  type        = "ingress"
  description = "Allow traffic from k3s server instance"

  protocol  = "TCP"
  from_port = var.db_port
  to_port   = var.db_port

  security_group_id        = var.db_security_group
  source_security_group_id = var.k3s_security_group
}
