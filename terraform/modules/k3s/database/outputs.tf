output "db_name" {
  value = postgresql_database.k3s_db.name
}

output "username" {
  value = postgresql_role.k3s_user.name
}

output "password" {
  value = postgresql_role.k3s_user.password

  sensitive = true
}
