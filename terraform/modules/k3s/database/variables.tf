variable "db_port" {
  type = number
}

variable "db_security_group" {
  type = string
}

variable "k3s_security_group" {
  type = string
}
