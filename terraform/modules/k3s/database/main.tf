terraform {
  required_providers {
    postgresql = {
      source  = "cyrilgdn/postgresql"
      version = ">= 1.11.0, < 2.0.0"
    }
  }
}

resource "random_password" "k3s_server_db_user_password" {
  length           = 16
  special          = true
  override_special = "!#%&*()-_+={}:;<>?"
}

resource "postgresql_role" "k3s_user" {
  name     = "k3s-server"
  login    = true
  password = random_password.k3s_server_db_user_password.result
}

resource "postgresql_database" "k3s_db" {
  name              = "k3s"
  connection_limit  = -1
  allow_connections = true

  owner = postgresql_role.k3s_user.name
}
