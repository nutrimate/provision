variable "vpc_id" {
  type = string
}

variable "lb_cidr_blocks" {
  type = list(string)
}

variable "lb_subnets" {
  type = list(string)
}

variable "k3s_server_ami_id" {
  type = string
}

variable "k3s_server_instance_type" {
  type = string
}

variable "k3s_server_subnets" {
  type = list(string)
}

variable "k3s_server_cidr_blocks" {
  type = list(string)
}

variable "db_port" {
  type = number
}

variable "db_endpoint" {
  type = string
}

variable "k3s_db_security_group" {
  type = string
}

variable "tags" {
  type = map(string)
}
