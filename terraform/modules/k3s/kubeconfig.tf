resource "null_resource" "wait_for_ec2_started" {
  depends_on = [module.k3s_servers]

  triggers = {
    launch_configuration_id = module.k3s_servers.this_launch_configuration_id
  }

  provisioner "local-exec" {
    command = "sleep 120"
  }
}

data "aws_instance" "k3s_server_instance" {
  filter {
    name   = "tag:Name"
    values = ["k3s-server"]
  }

  filter {
    name   = "image-id"
    values = [var.k3s_server_ami_id]
  }

  filter {
    name   = "instance-type"
    values = [var.k3s_server_instance_type]
  }

  filter {
    name   = "instance-state-code"
    values = [16]
  }

  depends_on = [null_resource.wait_for_ec2_started]
}

resource "null_resource" "copy_kubeconfig" {
  triggers = {
    instance_id = data.aws_instance.k3s_server_instance.id
  }

  provisioner "local-exec" {
    command = "rsync -e \"ssh -o StrictHostKeyChecking=no -i $KEY_PATH\" ec2-user@$PUBLIC_IP:/home/ec2-user/kubeconfig $ROOT/kubeconfig"

    environment = {
      KEY_PATH  = local.k3s_server_ssh_key_path
      ROOT      = path.root
      PUBLIC_IP = data.aws_instance.k3s_server_instance.public_ip
    }
  }
}
