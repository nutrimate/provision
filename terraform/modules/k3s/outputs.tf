output "lb_arn" {
  value = module.k3s_ingress_load_balancer.this_elb_arn
}

output "lb_dns_name" {
  value = module.k3s_ingress_load_balancer.this_elb_dns_name
}

output "lb_zone_id" {
  value = module.k3s_ingress_load_balancer.this_elb_zone_id
}
