module "k3s_server_security_group" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "k3s-server"
  description = "Security group for k3s-server instance"
  vpc_id      = var.vpc_id

  ingress_cidr_blocks = var.lb_cidr_blocks
  ingress_rules       = ["http-80-tcp", "https-443-tcp"]
  ingress_with_cidr_blocks = [
    {
      rule        = "ssh-tcp"
      cidr_blocks = "0.0.0.0/0"
    },
    {
      rule        = "kubernetes-api-tcp"
      cidr_blocks = "0.0.0.0/0"
    }
  ]
  ingress_with_self = [
    {
      from_port   = 8472
      to_port     = 8472
      protocol    = "udp"
      description = "Flannel VXLAN"
    },
    {
      from_port   = 10250
      to_port     = 10250
      protocol    = "tcp"
      description = "Kubelet metrics"
    }
  ]

  egress_rules = ["all-all"]

  tags = local.tags
}
