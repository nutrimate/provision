locals {
  tags = merge(var.base_tags, { component = "dietitian-web-app" })
}
