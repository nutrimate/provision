variable "env" {
  type        = string
  description = "Name of the environment to append to unique resources"
}

variable "domain" {
  type        = string
  description = "Domain where the app should be hosted"
}

variable "route53_zone_id" {
  type = string
}

variable "ci_deploy_group" {
  type        = string
  description = "IAM group where CI deployment users belong to"
}

variable "base_tags" {
  type = map(string)
}
