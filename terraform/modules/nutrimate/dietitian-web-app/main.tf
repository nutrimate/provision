provider "aws" {
  alias = "us-east-1"
}

module "website" {
  source = "../../shared/s3-website"

  component              = "dietitian-web-app"
  cloudfront_price_class = "PriceClass_100"
  env                    = var.env
  domain                 = var.domain
  route53_zone_id        = var.route53_zone_id
  ci_deploy_group        = var.ci_deploy_group

  providers = {
    aws           = aws
    aws.us-east-1 = aws.us-east-1
  }

  tags = local.tags
}
