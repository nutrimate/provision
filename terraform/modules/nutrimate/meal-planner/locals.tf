locals {
  function_name = "meal-planner"
  image_tag     = "c4253ff323d3327253a210f6c59c1433a363fed7"

  tags = merge(var.base_tags, { component = "meal-planner" })
}
