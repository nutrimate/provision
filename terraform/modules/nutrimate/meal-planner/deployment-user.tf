data "aws_caller_identity" "current" {
}
data "aws_region" "current" {
}

locals {
  region     = data.aws_region.current.name
  account_id = data.aws_caller_identity.current.account_id
  stack_name = "MealPlanner"
}

module "deploy_user" {
  source = "../../shared/deployment-user"

  component       = "meal-planner"
  ci_deploy_group = var.ci_deploy_group
  policy = jsonencode(
    {
      Version = "2012-10-17",
      Statement = [
        {
          Sid    = "GetAuthorizationToken",
          Effect = "Allow",
          Action = [
            "ecr:GetAuthorizationToken"
          ],
          Resource = "*"
        },
        {
          Effect = "Allow",
          Action = [
            "ecr:ListImages",
            "ecr:BatchCheckLayerAvailability",
            "ecr:GetDownloadUrlForLayer",
            "ecr:GetRepositoryPolicy",
            "ecr:DescribeRepositories",
            "ecr:ListImages",
            "ecr:DescribeImages",
            "ecr:BatchGetImage",
            "ecr:InitiateLayerUpload",
            "ecr:UploadLayerPart",
            "ecr:CompleteLayerUpload",
            "ecr:PutImage",
            "ecr:SetRepositoryPolicy",
            "ecr:GetRepositoryPolicy"
          ],
          Resource = [
            aws_ecr_repository.meal_planner.arn,
            "${aws_ecr_repository.meal_planner.arn}/"
          ]
        },
        {
          Action = [
            "lambda:UpdateFunctionCode"
          ],
          Effect = "Allow",
          Resource = [
            aws_lambda_function.meal_planner.arn
          ]
        }
      ]
  })

  tags = local.tags
}
