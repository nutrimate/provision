variable "env" {
  type        = string
  description = "Name of the environment to append to unique resources"
}

variable "ci_deploy_group" {
  type        = string
  description = "IAM group where CI deployment users belong to"
}

variable "base_tags" {
  type = map(string)
}
