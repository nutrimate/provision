output "ecr_repository_url" {
  value = aws_ecr_repository.meal_planner.repository_url
}

output "lambda_arn" {
  value = aws_lambda_function.meal_planner.arn
}
