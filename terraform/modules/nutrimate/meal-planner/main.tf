resource "aws_iam_role" "meal_planner" {
  name                  = local.function_name
  force_detach_policies = true

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      },
    ]
  })

  tags = local.tags
}

resource "aws_lambda_function" "meal_planner" {
  function_name = local.function_name

  role = aws_iam_role.meal_planner.arn

  image_uri    = "${aws_ecr_repository.meal_planner.repository_url}:${local.image_tag}"
  package_type = "Image"

  memory_size = 8192
  timeout     = 300

  environment {
    variables = {
      SENTRY_DSN = "https://6288bfea2b8d40b39438a2a3425487da@o479349.ingest.sentry.io/5524289"
      SENTRY_ENV = var.env
    }
  }

  tags = local.tags

  lifecycle {
    ignore_changes = [
      image_uri
    ]
  }
}

resource "aws_cloudwatch_log_group" "meal_planner" {
  name = "/aws/lambda/${local.function_name}"

  tags = local.tags
}

resource "aws_iam_policy" "logs" {
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "logs:PutLogEvents",
          "logs:CreateLogStream",
          "logs:CreateLogGroup",
        ]
        Effect = "Allow"
        Sid    = ""
        Resource = [
          "${aws_cloudwatch_log_group.meal_planner.arn}:*:*",
          "${aws_cloudwatch_log_group.meal_planner.arn}:*"
        ]
      },
    ]
  })
}

resource "aws_iam_policy_attachment" "meal_planner_logs" {
  name       = "${local.function_name}-logs"
  policy_arn = aws_iam_policy.logs.arn

  roles = [
    aws_iam_role.meal_planner.id
  ]
}
