locals {
  tags = merge(var.base_tags, { component = "dietitian-api" })
}
