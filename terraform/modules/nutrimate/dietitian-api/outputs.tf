output "db_name" {
  value = postgresql_database.dietitian_db.name
}

output "db_user" {
  value = postgresql_role.dietitian_user.name
}

output "db_password" {
  value = random_password.db_password.result

  sensitive = true
}

output "api_iam_user_access_key_id" {
  value = aws_iam_access_key.dietitian_api.id
}

output "api_iam_user_secret_key" {
  value = aws_iam_access_key.dietitian_api.secret

  sensitive = true
}
