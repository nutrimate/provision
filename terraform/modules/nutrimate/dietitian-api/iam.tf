resource "aws_iam_user" "dietitian_api" {
  name = "dietitian-api"

  force_destroy = true

  tags = local.tags
}

resource "aws_iam_user_policy" "policy" {
  name = "dietitian-api-call-meal-planner"
  user = aws_iam_user.dietitian_api.name

  policy = <<-EOF
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Action": [
          "lambda:InvokeFunction"
        ],
        "Effect": "Allow",
        "Resource": [
          "${var.meal_planner_function_arn}"
        ]
      }
    ]
  }
  EOF
}

resource "aws_iam_access_key" "dietitian_api" {
  user = aws_iam_user.dietitian_api.name
}
