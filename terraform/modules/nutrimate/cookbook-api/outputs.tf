output "db_name" {
  value = postgresql_database.cookbook_db.name
}

output "db_user" {
  value = postgresql_role.cookbook_user.name
}

output "db_password" {
  value = random_password.db_password.result
}
