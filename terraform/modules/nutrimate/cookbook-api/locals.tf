locals {
  tags = merge(var.base_tags, { component = "cookbook-api" })
}
