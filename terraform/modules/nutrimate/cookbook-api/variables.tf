variable "api_domain" {
  type = string
}

variable "route53_zone_id" {
  type = string
}

variable "k8s_lb_dns_name" {
  type = string
}

variable "k8s_lb_zone_id" {
  type = string
}

variable "base_tags" {
  type = map(string)
}
