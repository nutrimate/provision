terraform {
  required_providers {
    postgresql = {
      source  = "cyrilgdn/postgresql"
      version = ">= 1.11.0, < 2.0.0"
    }
  }
}

resource "random_password" "db_password" {
  length           = 16
  special          = true
  override_special = "!#$%&*()-_+={}:;<>?"
}

resource "postgresql_role" "cookbook_user" {
  name     = "cookbook-user"
  login    = true
  password = random_password.db_password.result
}

resource "postgresql_database" "cookbook_db" {
  name              = "cookbook"
  connection_limit  = -1
  allow_connections = true

  owner = postgresql_role.cookbook_user.name
}

resource "postgresql_extension" "uuid_ossp" {
  name = "uuid-ossp"

  database = postgresql_database.cookbook_db.name
}
