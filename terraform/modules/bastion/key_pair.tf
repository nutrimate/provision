resource "tls_private_key" "bastion_ssh_key" {
  algorithm = "RSA"
}

resource "aws_key_pair" "bastion_host" {
  key_name   = "${local.name}-key"
  public_key = tls_private_key.bastion_ssh_key.public_key_openssh
}

resource "local_file" "bastion_output_ssh_private_key" {
  filename = "${path.root}/bastion-ssh.pem"
  content  = tls_private_key.bastion_ssh_key.private_key_pem

  directory_permission = "0700"
  file_permission      = "0600"
}
