variable "vpc_id" {
  type = string
}

variable "subnet_id" {
  type = string
}

variable "domain" {
  type = string
}

variable "route53_zone_id" {
  type = string
}

variable "base_tags" {
  type = map(string)
}
