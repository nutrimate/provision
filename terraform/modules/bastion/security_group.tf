module "bastion_host_security_group" {
  source  = "terraform-aws-modules/security-group/aws//modules/ssh"
  version = ">= 3.17.0, < 4.0.0"

  vpc_id = var.vpc_id
  name   = "${local.name}-sg"

  ingress_cidr_blocks = [
    "0.0.0.0/0"
  ]

  tags = local.tags
}
