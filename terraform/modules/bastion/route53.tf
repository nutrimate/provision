resource "aws_route53_record" "bastion" {
  zone_id = var.route53_zone_id

  name    = "bastion.internal.${var.domain}"
  type    = "A"
  records = module.bastion_host.public_ip

  ttl = 300
}
