locals {
  name      = "bastion-host"
  user_data = <<EOF
#!/bin/bash
yum -y update
amazon-linux-extras install -y postgresql11
EOF

  tags = merge(var.base_tags, {
    component : local.name
  })
}
