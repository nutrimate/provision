module "bastion_host" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = ">= 2.17.0, < 3.0.0"

  name = local.name

  ami                         = data.aws_ami.latest_amazon_linux.image_id
  instance_type               = "t2.micro"
  associate_public_ip_address = true
  vpc_security_group_ids      = [module.bastion_host_security_group.this_security_group_id]
  subnet_id                   = var.subnet_id

  user_data_base64 = base64encode(local.user_data)
  key_name         = aws_key_pair.bastion_host.key_name

  root_block_device = [
    {
      volume_size           = "20"
      volume_type           = "gp2"
      delete_on_termination = true
    },
  ]

  tags = merge(local.tags, { Name = local.name })
}
