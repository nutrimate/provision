output "name" {
  value = local.name
}

output "security_group_id" {
  value = module.bastion_host_security_group.this_security_group_id
}
