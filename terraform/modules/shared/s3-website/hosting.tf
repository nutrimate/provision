locals {
  bucket_name = "nutrimate-${var.component}-${var.env}"
}

resource "aws_s3_bucket" "hosting" {
  bucket = local.bucket_name

  website {
    index_document = "index.html"
    error_document = "404.html"
  }

  policy = <<-EOF
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Sid": "PublicReadGetObject",
        "Effect": "Allow",
        "Principal": "*",
        "Action": [
          "s3:GetObject"
        ],
        "Resource": "arn:aws:s3:::${local.bucket_name}/*"
      }
    ]
  }
  EOF

  tags = var.tags
}

resource "aws_s3_bucket_public_access_block" "hosting_public_access_block" {
  bucket = aws_s3_bucket.hosting.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}
