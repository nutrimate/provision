provider "aws" {
  alias = "us-east-1"
}

locals {
  origin_id = "origin-bucket-${aws_s3_bucket.hosting.id}"
}

module "certificate" {
  source          = "../certificate"
  domain          = var.domain
  route53_zone_id = var.route53_zone_id

  providers = {
    aws = aws.us-east-1
  }

  tags = var.tags
}

resource "aws_cloudfront_distribution" "cdn" {
  enabled     = true
  price_class = var.cloudfront_price_class
  aliases     = concat([var.domain], var.other_domains)

  origin {
    domain_name = aws_s3_bucket.hosting.bucket_domain_name
    origin_id   = local.origin_id
  }

  default_root_object = "index.html"
  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = local.origin_id

    min_ttl     = 0
    default_ttl = 3600
    max_ttl     = 86400

    viewer_protocol_policy = "redirect-to-https"
    compress               = true

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    acm_certificate_arn      = module.certificate.arn
    ssl_support_method       = "sni-only"
    minimum_protocol_version = "TLSv1.1_2016"
  }

  custom_error_response {
    error_caching_min_ttl = 0
    error_code            = 403
    response_code         = 200
    response_page_path    = "/index.html"
  }

  tags = var.tags
}
