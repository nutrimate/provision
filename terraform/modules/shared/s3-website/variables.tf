variable "env" {
  type        = string
  description = "Name of the environment to append to unique resources"
}

variable "component" {
  type        = string
  description = "Name of the architectural component that will be included in resources names"
}

variable "domain" {
  type        = string
  description = "Domain where the app should be hosted"
}

variable "other_domains" {
  type        = list(string)
  description = "Additional domains where the app is hosted"
  default     = []
}

variable "route53_zone_id" {
  type = string
}

variable "cloudfront_price_class" {
  type = string
}

variable "ci_deploy_group" {
  type        = string
  description = "IAM group where CI deployment users belong to"
}

variable "tags" {
  type = map(string)
}
