module "deploy_user" {
  source = "../deployment-user"

  component       = var.component
  ci_deploy_group = var.ci_deploy_group
  policy          = <<-EOF
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Action": [
          "s3:PutObject",
          "s3:GetObject",
          "s3:ListBucket",
          "s3:DeleteObject",
          "s3:GetBucketLocation"
        ],
        "Effect": "Allow",
        "Resource": [
          "${aws_s3_bucket.hosting.arn}/*",
          "${aws_s3_bucket.hosting.arn}"
        ]
      }
    ]
  }
  EOF

  tags = var.tags
}
