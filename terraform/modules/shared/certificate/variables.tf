variable "domain" {
  type        = string
  description = "Primary certficiate domain"
}

variable "alternate_domains" {
  type        = list(string)
  description = "Alternate certificate domains"
  default     = []
}

variable "route53_zone_id" {
  type = string
}

variable "tags" {
  type = map(string)
}
