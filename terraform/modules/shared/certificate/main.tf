resource "aws_acm_certificate" "cert" {
  domain_name               = var.domain
  subject_alternative_names = var.alternate_domains
  validation_method         = "DNS"


  tags = var.tags
}

resource "aws_route53_record" "cert_record" {
  for_each = {
    for dvo in aws_acm_certificate.cert.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  zone_id = var.route53_zone_id
  name    = each.value.name
  type    = each.value.type
  records = [
    each.value.record
  ]
  ttl = 300
}
