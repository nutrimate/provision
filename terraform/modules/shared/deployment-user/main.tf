resource "aws_iam_user" "deploy_user" {
  name = "ci-deploy-${var.component}"

  force_destroy = true

  tags = var.tags
}

resource "aws_iam_user_group_membership" "deploy_group_membership" {
  user = aws_iam_user.deploy_user.name

  groups = [
    var.ci_deploy_group
  ]
}

resource "aws_iam_user_policy" "policy" {
  name = "ci-deploy-${var.component}-role"
  user = aws_iam_user.deploy_user.name

  policy = var.policy
}
