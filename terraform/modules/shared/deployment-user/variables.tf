variable "component" {
  type        = string
  description = "Name of the component which user can deploy"
}

variable "policy" {
  type        = string
  description = "IAM policy attached to the user"
}

variable "ci_deploy_group" {
  type        = string
  description = "IAM group where CI deployment users belong to"
}

variable "tags" {
  type = map(string)
}
