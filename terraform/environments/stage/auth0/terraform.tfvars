auth0_domain                   = "nutrimate-stage.eu.auth0.com"
api_domain                     = "api.stage.nutrimate.app"
dietitian_web_app_url          = "https://panel.stage.nutrimate.app"
dietitian_web_app_local_origin = "http://localhost:3000"
