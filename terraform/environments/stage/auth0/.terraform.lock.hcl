# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/alexkappa/auth0" {
  version     = "0.15.2"
  constraints = "0.15.2"
  hashes = [
    "h1:oQ3Y/tGjwt9K3ZDnzmZlotaxQhwgCh9Kce1pPcILIfE=",
    "zh:122c09d1478526515d3a3b738dbc9ab15ed323cacd6b71a5abbc0cdd6ccfbfbd",
    "zh:17fa7d3cfae1237e6dc77c9a4058d29db1014a47203e0b3d9181ae6deeb2f2b8",
    "zh:1b30136f22b4c31764c74d59e5dc7048bb6d3c060dc787a0b3a7bf69e2e63136",
    "zh:33aa66271a641234d50d711f70366e2440b353de84cbc7a2287bb39dab8b950b",
    "zh:3c760258d8e3b309b67dfee075a69ce603076e561ccfb82adf1a0add4b3eabe7",
    "zh:4a40b83b62842031fdb58a70484b9df4d1242c89e9f33e7b85538efcaf2d5d60",
    "zh:582ba9ae2fde1af34cf8f0eb541433b27223f5e8b904a7d8c0d2d59f0863a593",
    "zh:993d3e5c8f1a6d01c6f9151d8bda0b88b20454df7dccbc504a11d9ff83c1a83d",
    "zh:b899a879966c4170238980097a47ac94cb32ff6a9b24cff5c319e12abe492d3e",
    "zh:b91e9b5f2d8195d2a2b11ba6355313eb3f1b31669655933ab946fad22e3b6e0e",
    "zh:f18a817b072da3b2ebbe80f0ffe0f110698b148a1ec6a687304633bcd60cb9db",
  ]
}
