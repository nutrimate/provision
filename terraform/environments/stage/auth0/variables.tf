variable "auth0_domain" {
  type        = string
  description = "Auth0 domain name"
}

variable "auth0_client_id" {
  type        = string
  description = "Auth0 terraform service account client id"
}

variable "auth0_client_secret" {
  type        = string
  description = "Auth0 terraform service account client secret"
}

variable "dietitian_web_app_url" {
  type        = string
  description = "URL pointing at dietitian-web-app"
}

variable "dietitian_web_app_local_origin" {
  type        = string
  default     = ""
  description = "Additional origin on localhost domain that is able to access dietitian-web-app OAuth client"
}

variable "api_domain" {
  type        = string
  description = "Base domain of the API servers, e.g. api.nutrimate.app"
}
