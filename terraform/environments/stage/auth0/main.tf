terraform {
  backend "s3" {
    region = "eu-central-1"

    bucket = "nutrimate-terraform-state-stage"
    key    = "auth0.tfstate"
  }

  required_providers {
    auth0 = {
      source  = "alexkappa/auth0"
      version = "0.15.2"
    }
  }
}

provider "auth0" {
  domain        = var.auth0_domain
  client_id     = var.auth0_client_id
  client_secret = var.auth0_client_secret
}

module "auth0" {
  source = "../../../modules/auth0"

  dietitian_web_app = {
    web_origins   = compact([var.dietitian_web_app_url, var.dietitian_web_app_local_origin])
    logout_urls   = compact([var.dietitian_web_app_url, var.dietitian_web_app_local_origin])
    callback_urls = compact([var.dietitian_web_app_url, var.dietitian_web_app_local_origin])
  }

  api_url     = var.api_domain
  auth_domain = var.auth0_domain
}
