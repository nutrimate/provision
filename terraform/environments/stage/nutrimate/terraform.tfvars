env                  = "stage"
domain               = "stage.nutrimate.app"
route53_zone_id      = "Z03671242NNPUOMSPA2V3"
ci_deploy_user_group = "nutrimate-ci-deploy"
k8s_lb_dns_name      = "k3s-lb-290500454.eu-central-1.elb.amazonaws.com"
k8s_lb_zone_id       = "Z215JYRZR1TBD5"

db_tunnel_port = 7432

shared_db_master_username = "nutrimate_admin"
