variable "env" {
  type = string
}

variable "route53_zone_id" {
  type = string
}

variable "ci_deploy_user_group" {
  type = string
}

variable "domain" {
  type = string
}

variable "db_tunnel_port" {
  type = number
}

variable "shared_db_master_username" {
  type = string
}

variable "shared_db_master_password" {
  type = string
}

variable "k8s_lb_dns_name" {
  type = string
}

variable "k8s_lb_zone_id" {
  type = string
}
