terraform {
  backend "s3" {
    region = "eu-central-1"

    bucket = "nutrimate-terraform-state-stage"
    key    = "nutrimate.tfstate"
  }

  required_providers {
    aws = {
      version = ">= 3.31.0, < 4.0.0"
    }

    postgresql = {
      source  = "cyrilgdn/postgresql"
      version = ">= 1.11.0, < 2.0.0"
    }
  }
}

provider "aws" {
  region = "eu-central-1"
}

provider "aws" {
  alias  = "us-east-1"
  region = "us-east-1"
}

provider "postgresql" {
  host     = "localhost"
  port     = var.db_tunnel_port
  username = var.shared_db_master_username
  password = var.shared_db_master_password
  database = "postgres"

  superuser = false
}

# Nutrimate components

resource "aws_route53_record" "api" {
  name    = "*.${local.api_domain}"
  type    = "A"
  zone_id = var.route53_zone_id

  alias {
    evaluate_target_health = false
    name                   = var.k8s_lb_dns_name
    zone_id                = var.k8s_lb_zone_id
  }
}

module "cookbook_api" {
  source = "../../../modules/nutrimate/cookbook-api"

  api_domain      = local.api_domain
  k8s_lb_dns_name = var.k8s_lb_dns_name
  k8s_lb_zone_id  = var.k8s_lb_zone_id
  route53_zone_id = var.route53_zone_id

  base_tags = local.tags
}

module "meal_planner" {
  source = "../../../modules/nutrimate/meal-planner"

  env             = var.env
  ci_deploy_group = var.ci_deploy_user_group

  base_tags = local.tags
}

module "dietitian_api" {
  source = "../../../modules/nutrimate/dietitian-api"

  api_domain                = local.api_domain
  k8s_lb_dns_name           = var.k8s_lb_dns_name
  k8s_lb_zone_id            = var.k8s_lb_zone_id
  route53_zone_id           = var.route53_zone_id
  meal_planner_function_arn = module.meal_planner.lambda_arn

  base_tags = local.tags
}

module "dietitian_web_app" {
  source = "../../../modules/nutrimate/dietitian-web-app"

  providers = {
    aws           = aws
    aws.us-east-1 = aws.us-east-1
  }

  env             = var.env
  domain          = local.dietitian_web_app_domain
  route53_zone_id = var.route53_zone_id
  ci_deploy_group = var.ci_deploy_user_group

  base_tags = local.tags
}
