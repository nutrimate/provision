vpc_id = "vpc-038aa644427b21d98"
public_subnets = [
  "subnet-0617983841bacc11c",
  "subnet-0c9dc6d3340750351",
  "subnet-04045040d9e3fcf8d",
]
public_subnets_cidr_blocks = [
  "10.0.1.0/24",
  "10.0.2.0/24",
  "10.0.3.0/24",
]

bastion_host     = "3.120.225.252"
bastion_user     = "ec2-user"
bastion_key_path = "../core/bastion-ssh.pem"

db_tunnel_port = 7432

shared_db_endpoint          = "shared-db.cd65s3ssxphv.eu-central-1.rds.amazonaws.com:5432"
shared_db_port              = 5432
shared_db_master_username   = "nutrimate_admin"
shared_db_security_group_id = "sg-00e00ebbc4090323d"
