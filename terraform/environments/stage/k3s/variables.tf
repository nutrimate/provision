variable "vpc_id" {
  type = string
}

variable "public_subnets" {
  type = list(string)
}

variable "public_subnets_cidr_blocks" {
  type = list(string)
}

variable "bastion_host" {
  type = string
}

variable "bastion_user" {
  type = string
}

variable "bastion_key_path" {
  type = string
}

variable "shared_db_endpoint" {
  type = string
}

variable "shared_db_port" {
  type = number
}

variable "db_tunnel_port" {
  type = number
}

variable "shared_db_master_username" {
  type = string
}

variable "shared_db_master_password" {
  type = string
}

variable "shared_db_security_group_id" {
  type = string
}
