locals {
  tags = {
    env        = "stage"
    managed-by = "terraform"
  }
}
