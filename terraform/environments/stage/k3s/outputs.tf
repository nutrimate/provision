output "lb_dns_name" {
  value = module.k3s_server.lb_dns_name
}

output "lb_zone_id" {
  value = module.k3s_server.lb_zone_id
}
