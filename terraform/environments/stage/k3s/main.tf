terraform {
  backend "s3" {
    region = "eu-central-1"

    bucket = "nutrimate-terraform-state-stage"
    key    = "k3s.tfstate"
  }

  required_providers {
    aws = {
      version = ">= 3.26.0, < 4.0.0"
    }

    postgresql = {
      source  = "cyrilgdn/postgresql"
      version = ">= 1.11.0, < 2.0.0"
    }
  }
}

provider "aws" {
  region = "eu-central-1"
}

provider "postgresql" {
  host     = "localhost"
  port     = var.db_tunnel_port
  username = var.shared_db_master_username
  password = var.shared_db_master_password
  database = "postgres"

  superuser = false
}

module "k3s_server" {
  source = "../../../modules/k3s"

  vpc_id = var.vpc_id

  db_port     = var.shared_db_port
  db_endpoint = var.shared_db_endpoint

  k3s_db_security_group = var.shared_db_security_group_id

  k3s_server_instance_type = "t3a.small"
  k3s_server_ami_id        = "ami-0bd39c806c2335b95"

  k3s_server_subnets     = var.public_subnets
  k3s_server_cidr_blocks = var.public_subnets_cidr_blocks

  lb_cidr_blocks = var.public_subnets_cidr_blocks
  lb_subnets     = var.public_subnets

  tags = local.tags
}
