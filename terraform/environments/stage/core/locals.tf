locals {
  tags = {
    managed-by = "terraform"
    module     = "core"
  }
}
