terraform {
  backend "s3" {
    region = "eu-central-1"

    bucket = "nutrimate-terraform-state-stage"
    key    = "core.tfstate"
  }

  required_providers {
    aws = {
      version = ">= 3.26.0, < 4.0.0"
    }
  }
}

provider "aws" {
  region = "eu-central-1"
}

# Networking

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = ">= 2.66.0, < 3.0.0"

  name             = "main-vpc"
  cidr             = "10.0.0.0/16"
  azs              = ["eu-central-1a", "eu-central-1b", "eu-central-1c"]
  public_subnets   = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  private_subnets  = ["10.0.11.0/24", "10.0.12.0/24", "10.0.13.0/24"]
  database_subnets = ["10.0.21.0/24", "10.0.22.0/24", "10.0.23.0/24"]

  enable_nat_gateway = false
  single_nat_gateway = false

  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = local.tags
}

resource "aws_route53_zone" "main" {
  name = "${var.base_domain}."

  tags = local.tags
}

# Basic user configuration

module "users" {
  source = "../../../modules/users"
}

# Bastion host

module "bastion_host" {
  source = "../../../modules/bastion"

  vpc_id          = module.vpc.vpc_id
  subnet_id       = module.vpc.public_subnets[0]
  route53_zone_id = aws_route53_zone.main.id
  domain          = var.base_domain

  base_tags = local.tags
}

# Resources shred between multiple boundaries

module "shared_db" {
  source = "../../../modules/database"

  vpc_id     = module.vpc.vpc_id
  subnet_ids = module.vpc.database_subnets

  allowed_ingress_security_groups = [
    module.bastion_host.security_group_id
  ]

  base_tags = local.tags
}
