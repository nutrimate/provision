variable "base_domain" {
  type = string

  description = "Domain for which a Route53 zone will be created"
}
