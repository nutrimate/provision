resource "aws_route53_record" "google_mx_record" {
  zone_id = aws_route53_zone.main.id

  name = var.base_domain
  type = "MX"
  records = [
    "5 gmr-smtp-in.l.google.com",
    "10 alt1.gmr-smtp-in.l.google.com",
    "20 alt2.gmr-smtp-in.l.google.com",
    "30 alt3.gmr-smtp-in.l.google.com",
    "40 alt4.gmr-smtp-in.l.google.com",
  ]

  ttl = 3600
}
