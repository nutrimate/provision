output "vpc_id" {
  value = module.vpc.vpc_id
}

output "route53_zone_id" {
  value = aws_route53_zone.main.zone_id
}

output "public_subnets" {
  value = module.vpc.public_subnets
}

output "public_subnets_cidr_blocks" {
  value = module.vpc.public_subnets_cidr_blocks
}

output "db_endpoint" {
  value = module.shared_db.endpoint
}

output "db_user" {
  value = module.shared_db.master_username
}

output "db_password" {
  value = module.shared_db.master_password

  sensitive = true
}

output "db_security_group_id" {
  value = module.shared_db.security_group_id
}

output "ci_deploy_user_group" {
  value = module.users.ci_deploy_user_group
}
