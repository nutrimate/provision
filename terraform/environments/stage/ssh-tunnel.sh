#!/usr/bin/env bash

BASTION_HOST="bastion.internal.nutrimate.app"

BASTION_USER="ec2-user"
BASTION_KEY_PATH="./core/bastion-ssh.pem"

LOCAL_DB_TUNNEL_PORT=7432
SHARED_DB_ENDPOINT="shared-db.cd65s3ssxphv.eu-central-1.rds.amazonaws.com:5432"

ssh -i "${BASTION_KEY_PATH}" -N -L "${LOCAL_DB_TUNNEL_PORT}:${SHARED_DB_ENDPOINT}" "${BASTION_USER}@${BASTION_HOST}"
