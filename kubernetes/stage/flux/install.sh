#!/usr/bin/env bash

BASE_DIR=$(dirname -- "$0")

kubectl apply -f $BASE_DIR/namespace.yaml
kubectl apply -f $BASE_DIR/crds.yaml

helm repo add fluxcd https://charts.fluxcd.io
helm upgrade -i helm-operator fluxcd/helm-operator \
    --namespace flux \
    --set helm.versions=v3
