#!/usr/bin/env sh

SERVICE_ACCOUNT="ci-deployment"
NAMESPACE="kube-system"

SERVER=$(kubectl config view -o jsonpath='{.clusters[*].cluster.server}')

SECRET_NAME=$(kubectl --namespace=$NAMESPACE get sa $SERVICE_ACCOUNT -o=jsonpath='{.secrets[0].name}')
CA=$(kubectl --namespace=$NAMESPACE get secret/$SECRET_NAME -o jsonpath='{.data.ca\.crt}')
TOKEN=$(kubectl --namespace=$NAMESPACE get secret/$SECRET_NAME -o jsonpath='{.data.token}' | base64 --decode)

echo "apiVersion: v1
kind: Config
clusters:
- name: default-cluster
  cluster:
    certificate-authority-data: ${CA}
    server: ${SERVER}
contexts:
- name: default-context
  context:
    cluster: default-cluster
    namespace: default
    user: default-user
current-context: default-context
users:
- name: default-user
  user:
    token: ${TOKEN}
"
