# Cloud Weight Provisioning System

This repository holds terraform configuration of Cloud Weight infrastructure.

## Requirements
- Terraform @ 0.12

## Folder structure

### Terraform

`terraform` - folder holds terraform configuration of the stack
- `main.tf` - contains actual infrastructure stack configuration
- `env/` - contains env specific variables
- `modules/` - contains modules from which the stack is composed of
    - `components/` - contains simple modules that are reused multiple times
    - `services/` - contains specific configuration for individual services, e.g. landing, cookbook, auth.  
    - `shared/` - contains base infrastructure pieces that are shared between multiple services - e.g. API gateway, container registry, container orchestration.
